<?php

include('Parsedown.php');

# DEBUG (or use as 'start' by setting to '0')
if (!isset($_POST['lesson']))
    $_POST['lesson'] = 0;

$parsedown = new Parsedown();

$id = sprintf("lessons/%03d/",$_POST['lesson']);
$tips = @file_get_contents($id.'/tips');
$text = @file_get_contents($id.'/text');
$rule_data = explode("\n",trim(@file_get_contents($id.'/parser')));
$rules = array();

foreach ($rule_data as $i => $r)
{
    $data = explode(' # ',trim($r));
    if (count($data) < 4) continue;
    array_push($rules, array());
    $rules[$i]["name"] = trim($data[0]);
    $rules[$i]["value"] = trim($data[1]);
    $rules[$i]["match"] = trim($data[2]) == "true" ? true : false;
    $rules[$i]["required"] = trim($data[3]) == "true" ? true : false;
}

$code = @file_get_contents($id.'/code');

$data = json_encode(array('text'=>$parsedown->text($text), 'tips'=>$tips,
                          'rules'=>$rules, 'code'=>$code));

if ($text)
    print $data;
else
    http_response_code(404);

# DEBUG - comment out on release.
#print_r(json_decode($data));

?>
