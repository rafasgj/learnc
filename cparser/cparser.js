var Lexer = function() {
    this.buf = '';
    this.pos = 0;
    this.buflen = 0;
}

Lexer.prototype.input = function(buf) {
    this.buf = buf;
    this.pos = 0;
    this.buflen = buf.length;
    this.line = 1;
    this.linePos = 1;
}

Lexer.prototype.createToken = function(name, value) {
    return { name: name, value: value, line: this.line };
}

Lexer.prototype.multilineComment = function() {
    var start = this.pos;
    var end = this.multiLineComment();
    if (end < 0)
        throw Error("Error reading multiline comment.");
    return this.createToken('MULTILINE_COMMENT', this.buf.slice(start,end));
}

Lexer.prototype.lineComment = function() {
    var start = this.pos;
    var end = this.lineComment();
    return this.createToken('LINE_COMMENT', this.buf.slice(start,end));
}

Lexer.prototype.readStringLiteral = function(c, type, error) {
    this.nextChar();
    var start = this.pos;
    var end = this.readUntil(c);
    if (end < 0)
        throw Error(error);
    this.nextChar();
    return this.createToken(type, this.buf.slice(start,end));
}

Lexer.prototype.token = function() {
    // skip white space
    this.skip_ws();
    if (this.pos >= this.buflen)
        return null;

    var c = this.buf.charAt(this.pos);

    // Handle comments or OP_DIV
    if (c == '/') {
        var lookahead = this.buf.charAt(this.pos + 1);
        if (lookahead == '*') 
            return this.multilineComment();
        else
        if (lookahead == '/') 
            return this.lineComment();
        else // special case: OP_DIV
            return this.createToken('OP_DIV', '/');
    }
    // Handle strings
    if (c == '"')
        return this.readString(c,'LITERAL_STRING','Invalid String');
    if (c == "'")
        return this.readString(c,'LITERAL_CHAR','Invalid character literal.');
    // Handle preprocessor
    if (c == '#' && this.linePos == 1) {
        return this.preProcessor();
    }

    // operators
    var operator_list = '/+-*^(){}<>[].?:,;!=&|%';
    if (operator_list.indexOf(c) > -1) {
        this.nextChar();
        return this.checkOperator(c);
    }
  
    // Handle literal numbers.
    if (/[0-9]/.test(c)) {
        return readNumber;
    }

    // anything else is a 'word'
    var id = this.getId();
    if (id == null)
        throw Error("Invalid Identifier.");
    
    // if it is a reserved word (data type or keyword)
    var t = this.checkReservedWord(id);
    if (t) return t;

    // treat everything else as an identifier.
    return this.createToken('IDENTIFIER',id);
}

Lexer.prototype.preProcessor = function () {
    var id = this.getId();
    if (id == null)
        throw Error("Invalid Identifier.");
    if (id == "include") {
        id = this.getIncludeParameter();
        if (id == null)
            throw Error("Invalide #include parameter.");
        return this.createToken('CPP_INCLUDE',id);
    }
    throw Error("Invalid pre-processor directive.");
}

Lexer.prototype.readNumber = function () {
    var st = this.pos;
    while (this.pos < this.buflen && /[0-9]/.test(c)) {
        if (this.nextChar(true))
            throw Error("Invalid number format.");
        c = this.buf.charAt(this.pos);
    }
    if (c != '.') {
        // TODO: Treat literal modifiers (L, U, 0x, etc).
        return this.createToken('LITERAL_INT',this.buf.slice(st,this.pos));
    }
    if (this.nextChar(true))
            throw Error("Invalid number format.");
    while (this.pos < this.buflen && /[0-9]/.test(c)) {
        if (this.nextChar(true))
            throw Error("Invalid number format.");
        c = this.buf.charAt(pos);
    }
    // TODO: Treat literal modifiers (D, F).
    return this.createToken('LITERAL_FLOAT',this.buf.slice(st,this.pos));
}

Lexer.prototype.checkReservedWord = function (id)
{
    data_types = ["int", "float", "double", "char", "void",
                  "signed", "unsigned", "short", "long"];
    reserved = ["do","while","for","if", "else", "switch","case",
                "default", "break", "continue", "volatile", "register",
                "static", "extern", "const", "auto", "struct", "enum",
                "goto", "sizeof", "union", "typedef", "return" ];

    if (data_types.indexOf(id) > -1)
        return this.createToken('DATA_TYPE',id);
    var ndx = reserved.indexOf(id)
    if (ndx == -1) return null;
    switch (ndx) {
        case  0: return this.createToken('K_DO', id);
        case  1: return this.createToken('K_WHILE', id);
        case  2: return this.createToken('K_FOR', id);
        case  3: return this.createToken('K_IF', id);
        case  4: return this.createToken('K_ELSE', id);
        case  5: return this.createToken('K_SWITCH', id);
        case  6: return this.createToken('K_CASE', id);
        case  7: return this.createToken('K_DEFAULT', id);
        case  8: return this.createToken('K_BREAK', id);
        case  9: return this.createToken('K_CONTINUE', id);
        case 10: return this.createToken('K_VOLATILE', id);
        case 11: return this.createToken('K_REGISTER', id);
        case 12: return this.createToken('K_STATIC', id);
        case 13: return this.createToken('K_EXTERN', id);
        case 14: return this.createToken('K_CONST', id);
        case 15: return this.createToken('K_AUTO', id);
        case 16: return this.createToken('K_STRUCT', id);
        case 17: return this.createToken('K_ENUM', id);
        case 18: return this.createToken('K_GOTO', id);
        case 19: return this.createToken('K_SIZEOF', id);
        case 20: return this.createToken('K_UNION', id);
        case 21: return this.createToken('K_TYPEDEF', id);
        case 22: return this.createToken('K_RETURN', id);
        default: return null;
    }
}

Lexer.prototype.checkOperator = function (c)
{
    switch (c) {
        case '/':
            return this.createToket('OP_DIV',c);
        case '+':
            return this.createToken('OP_PLUS',c);
        case '-':
            return this.createToken('OP_MINUS',c);
        case '*':
            return this.createToken('OP_STAR',c);
        case '^':
            return this.createToken('OP_XOR',c);
        case '(':
            return this.createToken('OP_OPENPAR',c);
        case ')':
            return this.createToken('OP_CLOSEPAR',c);
        case '{':
            return this.createToken('OP_START',c);
        case '}':
            return this.createToken('OP_END',c);
        case '<':
            return this.createToken('OP_LESS',c);
        case '>':
            return this.createToken('OP_MORE',c);
        case '[':
            return this.createToken('OP_OPENBRAK',c);
        case ']':
            return this.createToken('OP_CLOSEBRAK',c);
        case '.':
            return this.createToken('OP_DOT',c);
        case '?':
            return this.createToken('OP_QUESTION',c);
        case ':':
            return this.createToken('OP_COLON',c);
        case ',':
            return this.createToken('OP_COMMA',c);
        case ';':
            return this.createToken('OP_SEMICOLON',c);
        case '!':
            return this.createToken('OP_NOT',c);
        case '=':
            return this.createToken('OP_EQUALS',c);
        case '&':
            return this.createToken('OP_AND',c);
        case '|':
            return this.createToken('OP_OR',c);
        case '%':
            return this.createToket('OP_MOD',c);
    }
    throw Error("Invalid operator: " + c);
}

Lexer.prototype.getIncludeParameter = function()
{
    this.skip_ws();
    c = this.buf.charAt(this.pos);
    if (c == '<') c = '>';
    else if (c != '"')
        return null;
    var st = this.pos;
    this.nextChar();
    while (this.pos < this.buflen && this.buf.charAt(this.pos) != c) {
        this.nextChar();
    }
    if (this.pos >= this.buflen)
        return null;
    return this.buf.slice(st, this.pos);
}

Lexer.prototype.getId = function()
{
    regex = new RegExp(/[a-z_0-9]+/i);
    var start = this.pos;
    c = this.buf.charAt(this.pos);
    if (/[^a-zA-Z_]/.test(c))
        return null;
    while (this.pos < this.buflen && regex.test(c)) {
        this.nextChar();
        c = this.buf.charAt(this.pos);
    }
    return this.buf.slice(start, this.pos);
}

Lexer.prototype.readUntil = function(c)
{
    while (this.pos < this.buflen) {
        c = this.buf.charAt(this.pos);
        if (c == c)
            return this.pos;
        if (c == '\n')
            return -1;
        this.nextChar();
    }
    return -1;
}

Lexer.prototype.lineComment = function ()
{
    while (this.pos < this.buflen)
        if (this.nextChar()) break;
    return this.pos;
}

Lexer.prototype.multiLineComment = function ()
{
    while (this.pos < this.buflen) {
        c = this.buf.charAt(this.pos);
        if (c == '*') {
            this.nextChar();
            if (this.pos >= this.buflen)
                return -1;
            if (this.buf.charAt(this.pos) == '/') {
                this.nextChar();
                return this.pos;
            }
        } else {
            this.nextChar();
        }
    }
    return -1;
}

Lexer.prototype.skip_ws = function()
{
    while (this.pos < this.buflen) {
        c = this.buf.charAt(this.pos);
        if (' \t\n\r'.indexOf(c) == -1)
            return;
        this.nextChar();
    }
}   

Lexer.prototype.nextChar = function(break_on_new_line)
{
    if (this.buf.charAt(this.pos) == '\n') {
        if (break_on_new_line)
            return true;
        this.line++;
        this.linePos = 0;
    }
    this.linePos++;
    this.pos++;

    return this.linePos == 1;
}

